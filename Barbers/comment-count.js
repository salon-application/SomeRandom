// In your "comment-count.js" file:
import html from "./utility.js";

class CustomerCommentCard extends HTMLElement {
    constructor() {
        super();
        this.counter = 0; // Initialize the counter to 0

        this.innerHTML = html`
            <div>(${this.counter})</div>
        `;
    }

    #Render() {
        this.innerHTML = html`
            <div>(${this.counter})</div>
        `;
    }

    AddToCounter() {
        this.counter++; // Increment the counter
        this.#Render();
    }

    connectedCallback() {
        // You can add any additional logic you need here
    }

    disconnectedCallback() {
        // You can add any cleanup logic here
    }
}

window.customElements.define('comment-count', CustomerCommentCard);
