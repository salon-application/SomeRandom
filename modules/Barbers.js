import "../Barbers/comment-count.js";
import "../Barbers/SumComment.js";

function getParameter(parameterName) {
  const parameters = new URLSearchParams(window.location.search);
  return parameters.get(parameterName);
}

class BarbersItem {
  constructor(news) {
    this.firstName = news.firstName;
    this.phone = news.phone;
    this.profile = news.profile;
    this.intro = news.intro;
    this.grade = news.grade;
    this.salon = news.salon;
    this.address = news.address;
  }
  // Inside BarbersItem class
  connectedCallback() {
    const card = this; // Reference to the card element
    card.addEventListener("click", () => {
      // Redirect to the details page with hairdresser's data
      window.location.href = `Barbers-details.html?firstName=${this.firstName}&phone=${this.phone}&profile=${this.profile}&intro=${this.intro}&grade=${this.grade}&salon=${this.salon}&address=${this.address}`;
    });

    // ... Rest of your code
  }

  Render() {
    return `
    <a class="hover_for_barber" style="text-decoration:none ; color:inherit
    " href="../Barbers/Barbers-detail.html?firstName=${this.firstName}&phone=${
      this.phone
    }&profile=${this.profile}&intro=${this.intro}&grade=${this.grade}&salon=${
      this.salon
    }&address=${this.address}" class="card-link">
        <div class="barbers-cards">
            <div class="barbers-cards-container">
                <div style="padding-bottom: 20px;">
                    <div style="display:flex">
                        <img style="border-radius:50%; width:100px;"
                            src=${this.profile}></img>
                        <div>
                            <h3 style="margin-left: 20px;">${
                              this.firstName
                            }</h3>
                            <div style="display:flex; justify-content:left; margin-left:15px">
                                ${this.generateRatingStars()}
                            </div>
                            <div style="margin-left: 20px;">
                                <p>Салон: <span>${this.salon}</span></p>
                                <p>Салбар: <span>${this.address}</span></p>
                                <p>Үнэлгээ:<span>${this.grade}</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <p>Холбогдох утас: <span>${this.phone}</span></p>
                    <p>Дотуур дугаар: 110</p>
                </div>
                <div>
                    <label style="display: flex;
                        margin-top: 10px;
                        margin-bottom: 10px;
                        font-weight: bold;
                        color: #18A0FB" for="Comment">Сэтгэгдэл
                        <comment-count></comment-count></label>
                    <br>
                    <p class="valuecomment" id="valueComment"></p>
                    <input style="box-sizing: border-box;
                        display: block;
                        width: 100%;
                        height: 30px;
                        border: 0.5px solid gray;
                        padding: calc(var(--size-bezel) * 1.5) var(--size-bezel);
                        background: transparent;
                        border-radius: 5px;" type="text" id="Comment" name="Comment" placeholder="Сэтгэгдэл бичих">
                    <button style="color: grey; border-radius: 5px;
                        width: 20%; height:25px;
                        margin-left: 57%;
                        margin-right: 5px ;" class="button buttonCancel">Reset</button>
                    <button style="color:#D39B59; border-radius: 5px;
                        width: 20%; height: 25px;
                        margin-top: 8px;
                        margin-right: 0px" class="button buttonSubmit" id="subButton">Submit</button>
                </div>
            </div>
        </div>
        </a>
        `;
  }

  generateRatingStars() {
    const maxRating = 5; // Maximum rating value
    const filledStars = Math.round(this.grade); // Number of filled stars
    console.log(filledStars);

    let starsHTML = " <br> ";

    for (let i = 1; i <= maxRating; i++) {
      if (i <= filledStars) {
        starsHTML += `<svg style="margin-left: 3px" width="20" height="20" viewBox="0 0 19 19" fill="#FADB14" xmlns="http://www.w3.org/2000/svg">
                    <path d="M17.4635 6.67501L12.0823 5.81017L9.67678 0.417196C9.61108 0.26954 9.50299 0.150009 9.36947 0.0773524C9.0346 -0.10546 8.62767 0.0468837 8.46024 0.417196L6.05471 5.81017L0.673523 6.67501C0.525165 6.69845 0.389522 6.77579 0.285671 6.89298C0.160121 7.03568 0.0909369 7.22767 0.0933214 7.42675C0.0957058 7.62583 0.169464 7.81572 0.298388 7.9547L4.19175 12.1524L3.27192 18.0797C3.25035 18.2176 3.26415 18.3594 3.31175 18.489C3.35935 18.6187 3.43885 18.731 3.54123 18.8132C3.64361 18.8955 3.76478 18.9443 3.891 18.9543C4.01722 18.9642 4.14343 18.9349 4.25533 18.8695L9.06851 16.0711L13.8817 18.8695C14.0131 18.9469 14.1657 18.9727 14.3119 18.9445C14.6807 18.8742 14.9287 18.4875 14.8651 18.0797L13.9453 12.1524L17.8386 7.9547C17.9446 7.83985 18.0145 7.68985 18.0357 7.52579C18.093 7.11563 17.8344 6.73595 17.4635 6.67501Z" />
                </svg>`;
      }

      // else {
      //     starsHTML += `<svg style="margin-left: 3px" width="20" height="20" viewBox="0 0 19 19" fill="grey" xmlns="http://www.w3.org/2000/svg">
      //         <path d="M17.4635 6.67501L12.0823 5.81017L9.67678 0.417196C9.61108 0.26954 9.50299 0.150009 9.36947 0.0773524C9.0346 -0.10546 8.62767 0.0468837 8.46024 0.417196L6.05471 5.81017L0.673523 6.67501C0.525165 6.69845 0.389522 6.77579 0.285671 6.89298C0.160121 7.03568 0.0909369 7.22767 0.0933214 7.42675C0.0957058 7.62583 0.169464 7.81572 0.298388 7.9547L4.19175 12.1524L3.27192 18.0797C3.25035 18.2176 3.26415 18.3594 3.31175 18.489C3.35935 18.6187 3.43885 18.731 3.54123 18.8132C3.64361 18.8955 3.76478 18.9443 3.891 18.9543C4.01722 18.9642 4.14343 18.9349 4.25533 18.8695L9.06851 16.0711L13.8817 18.8695C14.0131 18.9469 14.1657 18.9727 14.3119 18.9445C14.6807 18.8742 14.9287 18.4875 14.8651 18.0797L13.9453 12.1524L17.8386 7.9547C17.9446 7.83985 18.0145 7.68985 18.0357 7.52579C18.093 7.11563 17.8344 6.73595 17.4635 6.67501Z" fill="#FADB14"/>
      //     </svg>`;
      // }
    }

    return starsHTML;
  }

  connectedCallback() {
    this.querySelector("#subButton").addEventListener("click", () => {
      const count = this.querySelector("comment-count");
      count.AddToCounter(); // Increment the comment count
      const allcount = document.querySelector("all-comment");
      allcount.AddToCounter(this);
    });

    const event = new CustomEvent("newEvent", {
      detail: {},
      bubbles: false,
      cancelable: false,
    });
    this.dispatchEvent(event);
  }

  disconnectedCallback() {}

  attributeChangedCallback(attrName, oldVal, newVal) {}
}

export default class Barbers {
  constructor(recentNewsUrl) {
    this._recentNewsList = [];
    this._recentNewsUrl = recentNewsUrl;
    this._lastUpdated = Date.now();
    this._hasChanged = false;
  }

  Upload() {
    if (this._hasChanged) {
      fetch(this._recentNewsUrl, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json;charset=utf-8",
          versioning: false,
        },
        body: JSON.stringify(this._recentNewsList),
      })
        .then((response) => {
          console.log(response.status);
        })
        .catch((err) => {
          console.log(err);
        });

      this._hasChanged = false;
    }
  }

  Download(targetElement) {
    fetch(`${this._recentNewsUrl}/latest?meta=false`)
      .then((result) => {
        result.json().then((jsob) => {
          const filteredArray = jsob.filter(
            (newsItem) =>
              Date.parse(newsItem.grade) >
              Date.parse(
                getParameter("sort") === "grades"
                  ? "1"
                  : this._recentNewsList[this._recentNewsList.length - 1].grade
              )
          );

          if (filteredArray.length > 0) {
            filteredArray.sort(
              (a, b) => Date.parse(a.grade) - Date.parse(b.grade)
            ); // Sort by ascending "grade"

            gebi(targetElement).insertAdjacentHTML(
              "afterbegin",
              filteredArray
                .map((newNews) => {
                  const _newNews = new BarbersItem(newNews);
                  this._recentNewsList.push(_newNews);
                  return _newNews.Render();
                })
                .reduce((prevVal, curVal) => prevVal + curVal, "")
            );

            this._recentNewsList.forEach((newsItem) =>
              newsItem.Bind("input", "recentnews_title", "title")
            );
          }
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
}

const gebi = (id) => document.getElementById(id);

const barbers = new Barbers(
  "https://api.jsonbin.io/v3/b/644f6b319d312622a3555129"
);
barbers.Download("main");
